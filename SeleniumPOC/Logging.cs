﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using log4net.Config;
using log4net.Repository;

namespace SeleniumPOC
{
    class Logging
    {
        private ILog Log;

        public Logging(string LoggerName)
        {
            //Declare an instance for log4net

            var entryAssembly = Assembly.GetEntryAssembly();
            var logRepository = LogManager.GetRepository(entryAssembly);
            XmlConfigurator.Configure(logRepository, new FileInfo(fileName: "SeleniumPOC.dll.config"));
            if (!string.IsNullOrWhiteSpace(LoggerName))
                Log = LogManager.GetLogger(entryAssembly, LoggerName);
            else
                Log = LogManager.GetLogger((System.Reflection.MethodBase.GetCurrentMethod().DeclaringType));
        }

        public Logging(Type callingtype)
        {
            //Declare an instance for log4net
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo(fileName: "SeleniumPOC.dll.config"));
            Log = LogManager.GetLogger(callingtype);
        }

        public void LogInfo(string Method, string message)
        {
            //Main.LastInfo = Method + " " + message;
            Log.Info(Method + " " + message);
        }

        public void LogError(string Method, Exception ex, string CustomMessage)
        {
          //  Main.LastError = Method + " " + ex.Message + " " + CustomMessage;
            Log.Error(Method + " " + ex.Message + " " + CustomMessage);
        }

        public void LogDebug(string Method, string message)
        {
            Log.Debug(Method + " " + message);
        }

        public void LogFatal(string Method, string message)
        {
            Log.Fatal(Method + " " + message);
        }

        public void LogWarn(string Method, string message)
        {
            //Main.LastWarn = Method + " " + message;
            Log.Warn(Method + " " + message);
        }
    }
}


