﻿using System;
using NUnit;
using NUnit.Framework;
using Microsoft;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Threading;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using log4net;
using log4net.Config;
using log4net.Repository;


namespace SeleniumPOC
{
    public class LottoDefinition

    {
        //private const string DefaultLogger = "DefaultLogger";
        //private static Logging log = new Logging(DefaultLogger);
        private static Logging _log = new Logging(typeof(LottoDefinition));

        #region Members
        private readonly IWebDriver _driver;
        private const string Url = "https://qa.app.qa.tsretail.co.za";
        private const int WaitTime = 15;
        private WebDriverWait _wait;
        private const string LottoName = "Test South African Main Lotto";
        private IJavaScriptExecutor _jse;
        private readonly List<string> _colours = new List<string> { "green", "white", "yellow", "blue", "purple", "orange", "brown" };
        #endregion

        #region Constructors
        public LottoDefinition()
        {
            var option = new ChromeOptions();
          //  option.AddArgument("--headless");
            _driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), option);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(WaitTime);
        }
        #endregion


        #region Setup
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _driver.Navigate().GoToUrl(Url);
            Pageresize();
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(WaitTime));
            _jse = (IJavaScriptExecutor)_driver;
            Thread.Sleep(2000);
        }

        [SetUp]
        public void Setup()
        {

        }
        #endregion

        #region TearDown
        [OneTimeTearDown]
        public void Teardown()
        {
            _driver.Close();
            _driver.Quit();
        }
        #endregion

        #region Methods

        public void Pageresize()
        {
            _driver.Manage().Window.Maximize();
        }

        public void DoLogin()
        {
            var userNameElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.Id("username")));
            userNameElement.SendKeys("michael");

            var passwordElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.Id("password")));
            passwordElement.SendKeys("Password123!@#");

            var loginButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("kc-login")));
            loginButton.Click();
        }

        public void ClickMaintenance()
        {
            var maintain = _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("body > wagering-app > div > sidebar > div > sidebar-menu > div > div > sidebar-link:nth-child(3) > div")));
            _jse.ExecuteScript("arguments[0].click();", maintain);
        }

        public void Menu()
        {
            var menu = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"ts-bg-top-navbar\"]/div/div[1]/div/label/div")));
            _jse.ExecuteScript("arguments[0].click();", menu);
        }

        public void ClickLottoDef()
        {
            var action = new Actions(_driver);
            var lottoDef = _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("body > wagering-app > div > sidebar > div > sidebar-menu > div > div > div > sidebar-menu > div > div > sidebar-link:nth-child(1) > a > div > div.button-text")));
            action.MoveToElement(lottoDef);
            action.Click();
            action.Build().Perform();
        }

        public void AddLottoDef()
        {
            var action = new Actions(_driver);
            var addLottoDef = _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("body > lotto-app > div > div > div > game-definitions > div:nth-child(2) > div > div > div > div > button")));
            action.Click(addLottoDef);
            action.Build().Perform();
        }

        public void AddGrid()
        {
            var action = new Actions(_driver);
            var addGrid = _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("body > lotto-app > div > div > div > game-definition > div:nth-child(2) > div > div > div > div.sub-container > div:nth-child(2) > div > div.grid-nav-plus")));
            action.Click(addGrid);
            action.Build().Perform();
        }

        public void GridName()
        {
            const string gridName = "Main Grid";
            const string gridNamePath = "#ts-input-lotto-definition";
            var gridNamePathElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(gridNamePath)));
            gridNamePathElement.Clear();
            gridNamePathElement.SendKeys(gridName);
        }


        public void SelectNumbersTo()
        {
            const int size = 7;
            var previousGroupSize = 0;

            var elements = _driver.FindElements(By.TagName("ball"));
            _colours.ForEach(color =>
            {
                var action = new Actions(_driver);
                action.KeyDown(Keys.LeftControl);

                foreach (var ball in elements.Skip(previousGroupSize).Take(size))
                {
                    ball.Click();
                    CreateColourDefinition(color);
                }

                action.KeyUp(Keys.LeftControl);
                action.Build().Perform();

                previousGroupSize += size;
            });
        }

        public void CreateColourDefinition(string colour)
        {
            var select = new SelectElement(_driver.FindElement(By.CssSelector("select[id=ts-input-lotto-definition]")));
            select.SelectByText(colour);
        }

        public void BonusBallSelection()
        {
            var bonusBall = _driver.FindElements(By.TagName("draw-ball"))[6];
            bonusBall.Click();

            var action = new Actions(_driver);
            action.SendKeys(Keys.Tab);
            action.Build().Perform();
        }

        public void ActivateLotto()
        {
            //Lotto table. Fetch all rows in the table
            string xPath = "/html/body/lotto-app/div/div[2]/div/game-definitions/div[2]/div/div/table/tbody";
            List<IWebElement> listLottos = new List<IWebElement>(_driver.FindElements(By.XPath(xPath)));

            //Iterate through each row
            foreach (var lottos in listLottos)
            {
                //Fetch the columns from a particular row
                List<IWebElement> listElem = new List<IWebElement>(lottos.FindElements(By.TagName("Test South African Main Lotto")));
                if (listElem.Count > 0)
                {
                    //Iterate each column
                    foreach (var elemTd in listElem)
                    {

                    }
                }
            }
        }

        #endregion

        #region Tests
        [Test]
        public void CreateLottoDefinition()
        {
            #region variables
            const string lottoNamePath = "//*[@id=\"lotto-definition-name\"]";
            const string bonusBallDrawn = "Bonus Ball";
            const string saveButtonCss = "body > lotto-app > div > div > div > game-definition > div:nth-child(2) > div > div > div > div.row > div:nth-child(3) > button";
            #endregion



            DoLogin();
            Menu();
            ClickMaintenance();
            ClickLottoDef();

            Thread.Sleep(5000);

            AddLottoDef();

            //give the lotto a name
            var lottoNamePathElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(lottoNamePath)));
            lottoNamePathElement.SendKeys(LottoName);

            AddGrid();

            GridName();

            var action = new Actions(_driver);
            action.SendKeys(Keys.Tab);
            action.SendKeys(Keys.NumberPad4);
            action.SendKeys(Keys.NumberPad9);
            action.Build().Perform();

            SelectNumbersTo();

            var ballDrawn = _driver.FindElements(By.Id("ts-input-lotto-definition"))[3];
            ballDrawn.Click();
            ballDrawn.SendKeys(Keys.NumberPad7);
            Thread.Sleep(2000);

            BonusBallSelection();

            var action2 = new Actions(_driver);
            action2.SendKeys(Keys.Tab);

            action2.Build().Perform();
            var bonusBall = _driver.FindElements(By.Id("ts-input-lotto-definition"))[4];
            bonusBall.SendKeys(bonusBallDrawn);

            Thread.Sleep(1000);

            var saveButton = _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(saveButtonCss)));
            saveButton.Click();

            ActivateLotto();
        }

        #endregion
    }
    public class PlaceABet
    {
        private static readonly Logging Log = new Logging(typeof(PlaceABet));

        private string _ticketnumber;

        #region Members
        private readonly IWebDriver _driver;
        private const int WaitTime = 15;
        private WebDriverWait _wait;
        private IJavaScriptExecutor _jse;
        private Actions _action;
        private const string Url = "https://qa.app.qa.tsretail.co.za";
        private const string UserName = "michael";
        private const string Password = "Password123!@#";
        #endregion

        #region Constructors
        public PlaceABet()
        {
            var option = new ChromeOptions();
         //   option.AddArgument("--headless");
            _driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),option);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(WaitTime);
        }
        #endregion

        #region Setup and Teardown
        [OneTimeSetUp]
        public void Setup()
        {
            _driver.Navigate().GoToUrl(Url);
            Thread.Sleep(2000);
            _driver.Manage().Window.Maximize();
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(WaitTime));
            _jse = (IJavaScriptExecutor)_driver;
            _action = new Actions(_driver);
            Login();
        }

        [OneTimeTearDown]
        public void Teardown()
        {
            _driver.Close();
            _driver.Quit();
        }
        #endregion

        #region Methods

        public void Login()
        {
            var userNamePath = "//*[@id=\"username\"]";
            var userNameElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(userNamePath)));
            userNameElement.SendKeys(UserName);
            Thread.Sleep(1000);

            var passwordPath = "//*[@id=\"password\"]";
            var passwordElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(passwordPath)));
            passwordElement.SendKeys(Password);
            Thread.Sleep(1000);

            var loginButtonPath = "//*[@id=\"kc-login\"]";
            var loginButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(loginButtonPath)));
            loginButton.Click();
            Thread.Sleep(1000);
        }

        public void TopMenuButton()
        {
            var action = new Actions(_driver);
            var topMenu = _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#ts-bg-top-navbar > div > div.ts-hm.centering.border-right > div > label > div")));
            action.MoveToElement(topMenu);
            action.Click();
            action.Build().Perform();
        }

        public void SideMenuPlaceButton()
        {
            var placeButtonPath = "/html/body/wagering-app/div/sidebar/div/sidebar-menu/div/div/sidebar-link[1]/a/div/div[2]";
            var placeButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(placeButtonPath)));
            placeButton.Click();
        }

        public void Fixtures()
        {
            var clickFixtures = "/html/body/wagering-app/div/sidebar/div/sidebar-menu/div/div/sidebar-link[2]/a/div/div[2]";
            var clickFixturesButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(clickFixtures)));
            clickFixturesButton.Click();
        }
    
        public void Reports()
        {
            var clickReports = "/html/body/wagering-app/div/sidebar/div/sidebar-menu/div/div/sidebar-link[4]/div/div[2]";
            var clickReportsButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(clickReports)));
            clickReportsButton.Click();
        }

        public void BetReport()
        {
            var clickBetReports = "/html/body/wagering-app/div/sidebar/div/sidebar-menu/div/div/div/sidebar-menu/div/div/sidebar-link[1]/a/div/div[3]";
            var clickBetReportsButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(clickBetReports)));
            clickBetReportsButton.Click();
        }

        public void GenerateBetReport()
        {
            var generate = "//*[@id=\"generate\"]";
            var generateBetReportsButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(generate)));
            generateBetReportsButton.Click();
        }

        public void Company()
        {
            var company = "/html/body/wagering-app/div/main/bet-report/div/div[2]/bet-report-parameters/div/div/div/div/div[4]/select";
            var clickCompanyButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(company)));
            clickCompanyButton.Click();
            Thread.Sleep(500);
            var myCompany = "/html/body/wagering-app/div/main/bet-report/div/div[2]/bet-report-parameters/div/div/div/div/div[4]/select/option[2]";
            var myCompanyButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(myCompany)));
            myCompanyButton.Click();
            Thread.Sleep(500);
            var region = "/html/body/wagering-app/div/main/bet-report/div/div[2]/bet-report-parameters/div/div/div/div/div[5]/select";
            var regionButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(region)));
            regionButton.Click();
            Thread.Sleep(500);
            var myRegion = "/html/body/wagering-app/div/main/bet-report/div/div[2]/bet-report-parameters/div/div/div/div/div[5]/select/option[3]";
            var myRegionButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(myRegion)));
            myRegionButton.Click();
            Thread.Sleep(500);
            var allBranches = "/html/body/wagering-app/div/main/bet-report/div/div[2]/bet-report-parameters/div/div/div/div/div[6]/select";
            var branchesButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(allBranches)));
            branchesButton.Click();
            var myBranch = "/html/body/wagering-app/div/main/bet-report/div/div[2]/bet-report-parameters/div/div/div/div/div[6]/select/option[2]";
            var myBranchButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(myBranch)));
            myBranchButton.Click();
        }

        public void FixtureSearch()
        {
            var selectLottos = "/html/body/app/div/div/div/maintenance/div/div[2]/div[1]/div/div/div[2]/div[1]/ss-multiselect-dropdown/div/button";
            var selectLottosButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(selectLottos)));
            selectLottosButton.Click();
        }

        public void SelectSaLotto()
        {
            var selectSaLottoButton = _wait.Until(ExpectedConditions.ElementToBeClickable(By.LinkText("South African Main Lotto")));
            selectSaLottoButton.Click();
        }


        public void ChooseLottoStatus()
        {
            var status = "/html/body/app/div/div/div/maintenance/div/div[2]/div[1]/div/div/div[2]/div[2]/ss-multiselect-dropdown/div/button";
            var hitstatus = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(status)));
            hitstatus.Click();
        }

        public void SelectLottoStatus()
        {
            var openstatus = "/html/body/app/div/div/div/maintenance/div/div[2]/div[1]/div/div/div[2]/div[2]/ss-multiselect-dropdown/div/ul/li[7]/a/span[2]";
            var hitopenstatus = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(openstatus)));
            hitopenstatus.Click();
        }

        public void SuspendLotto()
        {
            var setstatus = "/html/body/app/div/div/div/maintenance/div/div[2]/div[2]/div/div/div[2]/search-results/ts-table/ts-tbody/ts-tr/search-result-item/ts-td[4]/ts-td-content/div/div[2]";
            var hitstatus = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(setstatus)));
            hitstatus.Click();

            var suspend = "/html/body/app/div/div/div/maintenance/div/div[2]/div[2]/div/div/div[2]/search-results/ts-table/ts-tbody/ts-tr/search-result-item/ts-td[4]/ts-td-editor/dropdown/div/typeahead-container/ul/li[1]/a/span";
            var hitsuspend = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(suspend)));
            hitsuspend.Click();

            var suspendyes = "/html/body/modal-container/div/div/div[2]/button[2]";
            var hitsuspendyes = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(suspendyes)));
            hitsuspendyes.Click();
        }

        public void CloseLotto()
        {
            var setstatus = "/html/body/app/div/div/div/maintenance/div/div[2]/div[2]/div/div/div[2]/search-results/ts-table/ts-tbody/ts-tr/search-result-item/ts-td[4]/ts-td-content/div/div[2]";
            var hitstatus = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(setstatus)));
            hitstatus.Click();

            var close = "/html/body/app/div/div/div/maintenance/div/div[2]/div[2]/div/div/div[2]/search-results/ts-table/ts-tbody/ts-tr/search-result-item/ts-td[4]/ts-td-editor/dropdown/div/typeahead-container/ul/li[2]/a/span";
            var hitclose = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(close)));
            hitclose.Click();

            var closeyes = "/html/body/modal-container/div/div/div[2]/button[2]";
            var hitcloseyes = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(closeyes)));
            hitcloseyes.Click();
        }

        public void SaLottoResult()
        {
            var lottoresult = "/html/body/app/div/div/div/maintenance/div/div[2]/div[2]/div/div/div[2]/search-results/ts-table/ts-tbody/ts-tr/search-result-item/ts-td[5]/ts-td-content/button[1]";
            var hitlottoresult = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(lottoresult)));
            hitlottoresult.Click();
        }

        public void SendSaLottoResult()
        {
            var firstball = "/html/body/lotto-app/div/div/div/resulting/div[2]/div/ball-selector/div/div/div/div/div[2]/lotto-ball[1]";
            var enterfirstball = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(firstball)));
            EnterAlottoNumber(enterfirstball, Keys.NumberPad1,true);
            
            var secondball = "/html/body/lotto-app/div/div/div/resulting/div[2]/div/ball-selector/div/div/div/div/div[2]/lotto-ball[2]";
            var entersecondball = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(secondball)));
            EnterAlottoNumber(entersecondball, "2", true);

            var thirdball = "/html/body/lotto-app/div/div/div/resulting/div[2]/div/ball-selector/div/div/div/div/div[2]/lotto-ball[3]";
            var enterthirdball = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(thirdball)));
            EnterAlottoNumber(enterthirdball, "8", true);

            var fourthball = "/html/body/lotto-app/div/div/div/resulting/div[2]/div/ball-selector/div/div/div/div/div[2]/lotto-ball[4]";
            var enterfourthball = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(fourthball)));
            EnterAlottoNumber(enterfourthball, "30", false);

            var fifthball = "/html/body/lotto-app/div/div/div/resulting/div[2]/div/ball-selector/div/div/div/div/div[2]/lotto-ball[5]";
            var enterfifthball = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(fifthball)));
            EnterAlottoNumber(enterfifthball, "3", true);

            var sixball = "/html/body/lotto-app/div/div/div/resulting/div[2]/div/ball-selector/div/div/div/div/div[2]/lotto-ball[6]";
            var entersixball = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(sixball)));
            EnterAlottoNumber(entersixball, "7", true);

            var sevenball = "/html/body/lotto-app/div/div/div/resulting/div[2]/div/ball-selector/div/div/div/div/div[2]/lotto-ball[6]";
            var entersevenball = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(sevenball)));
            EnterAlottoNumber(entersevenball, "9", true);

            var submit = "/html/body/lotto-app/div/div/div/resulting/div[2]/div/div[2]/div[2]/button";
            var hitsubmit = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(submit)));
            hitsubmit.Click();

        }

        public void LottoAbandon()
        {
            var abandon = "/html/body/app/div/div/div/maintenance/div/div[2]/div[2]/div/div/div[2]/search-results/ts-table/ts-tbody/ts-tr/search-result-item/ts-td[5]/ts-td-content/button[2]";
            var hitabandon = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(abandon)));
            hitabandon.Click();
        }

        public void AbandonConfirmation()
        {
            var yes = "/html/body/lotto-app/div/div/div/resulting/div/div/div/div[2]/div/div[2]/div[2]/a";
            var hityes = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(yes)));
            hityes.Click();
        }

        public void SideMenuFixtureMaintenanceButton()
        {
            var fixtureMaintenanceButtonPath = "/html/body/app/div/div[1]/sidebar/div/sidebar-menu/div/div/sidebar-link[2]/a/div/div[2]";
            var fixture = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(fixtureMaintenanceButtonPath)));

            var script = "arguments[0].click();";
            _jse.ExecuteScript(script, fixture);
        }

        public void SideMenuMaintenanceButton()
        {
            var maintenanceButtonPath = "/html/body/wagering-app/div/sidebar/div/sidebar-menu/div/div/sidebar-link[3]/div/div[2]";
            var maintain = _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(maintenanceButtonPath)));

            var script = "arguments[0].click();";
            _jse.ExecuteScript(script, maintain);
        }

        public string GetPunterTotal()
        {
            //Get Total Stake value
            var totalStakePath = "/html/body/wagering-app/div/main/wagering/div/div[2]/div/betslip/tabset/div/tab/singles-betslip/div[2]/div/ts-table/ts-thead/ts-tr/ts-td[6]/ts-td-content/div/div";
            var totalStakeElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(totalStakePath)));

            return totalStakeElement.Text;
        }

        public void TicketLookUpSearch()
        {
            var searchTicket = "//*[@id=\"search-box\"]";
            var searchElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(searchTicket)));
            searchElement.Click();
            IJavaScriptExecutor jse = (IJavaScriptExecutor)_driver;
            jse.ExecuteScript("arguments[0].value='" + "-t " + _ticketnumber + "';", searchElement);
            searchElement.SendKeys(Keys.Enter);
            searchElement.SendKeys(Keys.Enter);
        }

        public void CancelBet()
        {
            var cancel = "/html/body/wagering-app/div/main/wagering/div/div[1]/div/div/div[2]/search-results/ts-table/ts-tbody/ts-tr[1]/ticket-result-item/ts-td[6]/ts-td-content/div/div/ts-td/ts-td-content/span[1]";
            var hitcancel = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(cancel)));
            hitcancel.Click();
        }

        public void CancelAllBets()
        {
            var cancelall = "/html/body/wagering-app/div/main/wagering/div/div[1]/div/div/div[2]/search-results/ts-table/ts-tbody/ts-tr[1]/ticket-result-item/ts-td[6]/ts-td-content/div/div/ts-td/ts-td-content/span[1]";
            var hitcancelall = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(cancelall)));
            hitcancelall.Click();
        }

        public void CancelYes()
        { 
            var buttons = _driver.FindElements(By.CssSelector("body > modal-container > div > div > cancel-dialog > div.modal-footer > button.btn.btn-primary"));
            buttons[0].Click();
        }


        public bool SearchLottoFixture(string fixture)
        {

            var searchElementPath = "//*[@id=\"search-box\"]";
            var searchElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(searchElementPath)));
            searchElement.Clear();
            searchElement.Click();
            searchElement.SendKeys(fixture);

            Thread.Sleep(2000);

            _action.MoveToElement(searchElement);
            _action.SendKeys(Keys.ArrowDown).Perform();

            Thread.Sleep(2000);

            var fixtureElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/wagering-app/div/main/wagering/div/div[1]/div/div/div[2]/search-results/ts-table/ts-tbody/ts-tr/search-result-item/lt003-search-result-item/ts-td")));
            fixtureElement.Click();
            fixtureElement.SendKeys(Keys.Enter);

            return true;
        }


        public void EnterAlottoNumber(IWebElement ballSelector, string numberToEnter, bool includeEnterAfter)
        {
            _action = new Actions(_driver);
            _action.MoveToElement(ballSelector);
            _action.Click(ballSelector);
            _action.SendKeys(ballSelector, numberToEnter);

            if (includeEnterAfter)
            {
                _action.SendKeys(Keys.Enter);
            }
            _action.Build().Perform();

        }

        public void EnterLottoNumbers()
        {
            var ballSelector1Path = "body > wagering-app > div > main > wagering > div > div.row.vspace.flex-auto > div > div > div.popover-content.max-popover-content > search-results > ts-table > ts-tbody > ts-tr > search-result-item > lt003-search-result-item > ts-td > ts-td-editor > div > div.editor-container > lt003-editor > lt003-betslip-ball-selector > div > div.ball-container > lt003-lotto-line-cursor";
            var ballSelector1 = _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(ballSelector1Path)));

            EnterAlottoNumber(ballSelector1, Keys.NumberPad1, false);
            Thread.Sleep(1000);

            var ballSelector2Path = "body > wagering-app > div > main > wagering > div > div.row.vspace.flex-auto > div > div > div.popover-content.max-popover-content > search-results > ts-table > ts-tbody > ts-tr > search-result-item > lt003-search-result-item > ts-td > ts-td-editor > div > div.editor-container > lt003-editor > lt003-betslip-ball-selector > div > div.ball-container > lt003-lotto-line-cursor:nth-child(3)";
            var ballSelector2 = _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(ballSelector2Path)));

            EnterAlottoNumber(ballSelector2, Keys.NumberPad2, false);
            Thread.Sleep(1000);

            var ballSelector3Path = "body > wagering-app > div > main > wagering > div > div.row.vspace.flex-auto > div > div > div.popover-content.max-popover-content > search-results > ts-table > ts-tbody > ts-tr > search-result-item > lt003-search-result-item > ts-td > ts-td-editor > div > div.editor-container > lt003-editor > lt003-betslip-ball-selector > div > div.ball-container > lt003-lotto-line-cursor:nth-child(5)";
            var ballSelector3 = _wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(ballSelector3Path)));

            EnterAlottoNumber(ballSelector3, Keys.NumberPad3, true);
            Thread.Sleep(2000);

        }

        public void SetFocusToSubmitButton()
        {
            var addMoreButtonPath = "/html/body/wagering-app/div/main/wagering/div/div[2]/div/betslip/tabset/div/tab/singles-betslip/div[3]/div/div[2]/button";
            var addMoreElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(addMoreButtonPath)));
            _action = new Actions(_driver);
            _action.SendKeys(Keys.ArrowRight);
            _action.Build().Perform();
        }

        public void EnterBetStake()
        {
            var stakeElementPath = "/html/body/wagering-app/div/main/wagering/div/div[2]/div/betslip/tabset/div/tab/singles-betslip/div[1]/div/ts-table/ts-tbody/div/ts-tr/div/div/ts-td[5]/ts-td-content/stake-input/input";
            var stakeElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(stakeElementPath)));

            _action = new Actions(_driver);
            _action.MoveToElement(stakeElement);
            _action.SendKeys(Keys.NumberPad8);
            _action.SendKeys(Keys.Enter);
            _action.Build().Perform();

            Thread.Sleep(1000);

            SetFocusToSubmitButton();
        }

        public void SubmitBet()
        {
            var submitButtonPath = "/html/body/wagering-app/div/main/wagering/div/div[2]/div/betslip/tabset/div/tab/singles-betslip/div[3]/div/div[3]/button";
            var submitBetElement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(submitButtonPath)));

            _action = new Actions(_driver);
            _action.SendKeys(Keys.Enter);
            _action.Build().Perform();
        }

        public void GetTicketNumber()
        {
            var selector = "/html/body/wagering-app/div/main/bet-report/div/div[2]/bets-data-table/ng2-smart-table/table/tbody/tr[2]/td[4]/ng2-smart-table-cell/table-cell-view-mode/div/div";
            var ticketnumberelement = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(selector)));
            _ticketnumber = ticketnumberelement.Text;
            Console.WriteLine(_ticketnumber);          
        }

        public void MainBody()
        {
            var body = "/html/body/app/div/div";
            var bodyclick = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(body)));
            bodyclick.Click();
        }



        #endregion


        [Test]
        public void PlaceALottoBet()
        {
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "Test STARTED");
            Thread.Sleep(1000);

            for (var i = 0; i < 5; i++)
            {
                Thread.Sleep(5000);


                SearchLottoFixture("sou");
                Log.LogInfo(MethodBase.GetCurrentMethod().Name, "Fixture found");


                Thread.Sleep(1000);
                EnterLottoNumbers();
                Thread.Sleep(1000);

                EnterBetStake();
                Log.LogInfo(MethodBase.GetCurrentMethod().Name, "R8 stake entered");
                Thread.Sleep(1000);

                SubmitBet();
                Thread.Sleep(1000);
            }


            Assert.AreEqual("40.00", GetPunterTotal());
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "R40 bet Successful");

            Thread.Sleep(2000);
           
        }

        [Test]
        public void TicketLookup()
        {
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "Test STARTED");
            TopMenuButton();
            Thread.Sleep(1000);
            Reports();
            Thread.Sleep(1000);
            BetReport();
            Company();
            GenerateBetReport();
            GetTicketNumber();
            Thread.Sleep(1000);
            SideMenuPlaceButton();
            TicketLookUpSearch();
            Thread.Sleep(1000);
            
        }

        [Test]
        public void LottoResult()
        {
            TopMenuButton();
            Thread.Sleep(1000);
            Fixtures();
            Thread.Sleep(1000);
            FixtureSearch();
            SelectSaLotto();
            ChooseLottoStatus();
            SelectLottoStatus();
            SaLottoResult();
            Thread.Sleep(5000);
            SendSaLottoResult();
           
        }

        [Test]
        public void LottoResultSuspend()
        {
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "Test STARTED");
            TopMenuButton();
            Thread.Sleep(1000);
            Fixtures();
            Thread.Sleep(1000);
            FixtureSearch();
            SelectSaLotto();
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "Found SA Lotto");
            ChooseLottoStatus();
            SelectLottoStatus();
            SuspendLotto();
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "SA Lotto Suspended");
        }

        [Test]
        public void LottoResultClose()
        {
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "Test STARTED");
            TopMenuButton();
            Thread.Sleep(1000);
            Fixtures();
            Thread.Sleep(1000);
            FixtureSearch();
            SelectSaLotto();
            ChooseLottoStatus();
            SelectLottoStatus();
            CloseLotto();
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "SA Lotto Closed");
        }

        [Test]
        public void LottoResultAbandon()
        {
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "Test STARTED");
            TopMenuButton();
            Thread.Sleep(1000);
            Fixtures();
            Thread.Sleep(1000);
            FixtureSearch();
            SelectSaLotto();
            ChooseLottoStatus();
            SelectLottoStatus();
            LottoAbandon();
            AbandonConfirmation();
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "SA Lotto Abandoned");
        }

        [Test]
        public void CancelSingleBet()
        {
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "Test STARTED");
            TopMenuButton();
            Thread.Sleep(1000);
            Reports();
            Thread.Sleep(1000);
            BetReport();
            Company();
            GenerateBetReport();
            GetTicketNumber();
            Thread.Sleep(1000);
            SideMenuPlaceButton();
            TicketLookUpSearch();
            Thread.Sleep(1000);
            CancelBet();
            CancelYes();
            
        }

        [Test]
        public void CancelBets()
        {
            Log.LogInfo(MethodBase.GetCurrentMethod().Name, "Test STARTED");
            TopMenuButton();
            Thread.Sleep(1000);
            Reports();
            Thread.Sleep(1000);
            BetReport();
            Company();
            GenerateBetReport();
            GetTicketNumber();
            Thread.Sleep(1000);
            SideMenuPlaceButton();
            TicketLookUpSearch();
            Thread.Sleep(1000);
            CancelAllBets();
            CancelYes();
            
        }
    }
}




